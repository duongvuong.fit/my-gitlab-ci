terraform {
  backend "s3" {
    encrypt = "true"
    bucket         = "vuongdv6-s3-backend"
    key            = "vuongdv6/s3-backend/vuongdv6.tfstate"
    region         = "us-east-1"
    dynamodb_table = "vuongdv6-s3-backend"
  }
}


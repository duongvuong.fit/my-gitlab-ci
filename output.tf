output "public_ip_node-1" {
  value = aws_instance.node-1.public_ip
}

output "eip_public_ip" {
  value = aws_eip.eip.public_ip
}

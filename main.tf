# Terraform settings 
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  required_version = ">= 0.14.9"
}

# 0. Define Terraform provider
provider "aws" {
  region = "us-east-1"
}

# 1. Create a VPC
resource "aws_vpc" "production-vpc" {
  cidr_block = var.vpc_cidr_block
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
  tags = {
    "Name" = "production"
  }
}

# 2. Create internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.production-vpc.id
  tags = {
    "Name" = "production-gw"
  }
}

# 3. Create an Elastic IP
resource "aws_eip" "eip" {
  vpc = true
  tags = {
    "Name" = "product-eip"
  }
}

# 4. Create Nat Gateway
resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.eip.id
  subnet_id = aws_subnet.private-subnet.id
}

# 3. Create route table
resource "aws_route_table" "public-route-table" {
  vpc_id = aws_vpc.production-vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
  }
  tags = {
    "Name" = "public-route-table"
  }
}

resource "aws_route_table" "private-route-table" {
  vpc_id = aws_vpc.production-vpc.id
  route {
      cidr_block = "0.0.0.0/0"
      nat_gateway_id = aws_nat_gateway.nat_gateway.id
  }
  tags = {
    "Name" = "private-route-table"
  }
}

# 4. Create a Subnet
resource "aws_subnet" "public-subnet" {
  vpc_id = aws_vpc.production-vpc.id
  cidr_block = "10.0.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    "Name" = "public-prod-subnet"
  }
}

resource "aws_subnet" "private-subnet" {
  vpc_id = aws_vpc.production-vpc.id
  cidr_block = "10.0.2.0/24"
  availability_zone = "us-east-1a"
  tags = {
    "Name" = "private-prod-subnet"
  }
}

# 5. Associate subnet with route table
resource "aws_route_table_association" "public-associate" {
  subnet_id = aws_subnet.public-subnet.id
  route_table_id = aws_route_table.public-route-table.id
}

resource "aws_route_table_association" "private_associate" {
  subnet_id = aws_subnet.private-subnet.id
  route_table_id = aws_route_table.private-route-table.id
}
# 6. Create Security Group to allow ports 22, 443, 80
resource "aws_security_group" "allow-connected" {
  name = "allow_web_traffic"
  vpc_id = aws_vpc.production-vpc.id

  ingress {
      description = "HTTPS"
      from_port = 443
      to_port = 443
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      description = "HTTP"
      from_port = 80
      to_port = 80
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
      description = "SSH"
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
      from_port = 0
      to_port = 0
      protocol = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "node-1" {
  ami = "ami-09d56f8956ab235b3"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.public-subnet.id
  security_groups = [aws_security_group.allow-connected.id]
  key_name = "VuongDV6"
  tags = {
    "Name" = "production-node-1"
  }
}

















